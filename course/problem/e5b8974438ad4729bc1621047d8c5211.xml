<problem display_name="Test 3.4" markdown="null">
  <p>
	We are going to create a figure presenting a random walk in 3D.
	The movement is decided randomly according to the following rules:<pre><code>% r  &lt; 1/6      : the displacement is done parallel to the x-axis, towards positive values
% 1/6 &lt; r &lt; 2/6 : the displacement is done parallel to the x-axis, towards negative values
% 2/6 &lt; r &lt; 3/6 : the displacement is done parallel to the y-axis, towards positive values
% 3/6 &lt; r &lt; 4/6 : the displacement is done parallel to the y-axis, towards negative values
% 4/6 &lt; r &lt; 5/6 : the displacement is done parallel to the z-axis, towards positive values
% 5/6 &lt; r       : the displacement is done parallel to the z-axis, towards negative values</code></pre>
  </p>
  <p>
    You must complete the following code :
	</p>
  <pre>
    <code>% number of steps to take:
steps = 20000;

% First we must generate "steps" random draws
% between 0 and 1 following a uniform distribution
[TO COMPLETE #1]

% with the following commands, elementwise,
% the direction of each step can be calculated
 
dx = (r  &lt; 1/6) - ( r &gt; 1/6 &amp; r  &lt;2/6);
dy = (r &gt; 2/6 &amp; r  &lt; 3/6) - ( r &gt; 3/6 &amp; r  &lt;4/6);
[TO COMPLETE #2]

% To sum all steps, one can create the lower triangular matrix
% with only ones below the diagonal
A = tril(ones(steps,steps),-1);

% The incremental sum of each step is performed by A*dx, etc:
[TO COMPLETE #3]

% now (x,y,z) define the successive positions of the random walk

% This walk can be plotted
[TO COMPLETE #4]</code>
  </pre>
  <p>
    <br/>
  </p>
  <p>TO COMPLETE #1 :</p>
  <optionresponse>
    <optioninput>
      <option correct="False">r = rand(1,steps);</option>
      <option correct="False">r = rand(steps,step);</option>
      <option correct="True">r = rand(steps,1);</option>
      <option correct="False">r = rand(steps);</option>
      <option correct="False">r = rand();</option>
    </optioninput>
  </optionresponse>
  <p>TO COMPLETE #2 :</p>
  <choiceresponse>
    <checkboxgroup>
      <choice correct="false">dz = (4/6 &gt; r &amp; r  &lt; 5/6) - ( 5/6 &gt; r);</choice>
      <choice correct="true">dz = (4/6 &lt; r &amp; r  &lt; 5/6) - ( 5/6 &lt; r);</choice>
      <choice correct="false">dz = (r &lt; 4/6 &amp; r  &lt; 5/6) - ( r &lt; 5/6);</choice>
      <choice correct="false">dz = (4/6 &lt; r &amp; r  &lt; 5/6) - ( 6/6 &lt; r);</choice>
      <choice correct="false">dz = (r &lt; 4/6 &amp;&amp; r  &lt; 5/6) - ( r &lt; 5/6);</choice>
      <choice correct="false">dz = (4/6 &lt; r &amp; r  &lt; 5/6) - ( 6/6 &gt; r);</choice>
    </checkboxgroup>
  </choiceresponse>
  <p>TO COMPLETE #3 :</p>
  <optionresponse>
    <optioninput>
      <option correct="false">x = A.*dx; y = A.*dy; z = A.*dz;</option>
      <option correct="true">x = A*dx; y = A*dy; z = A*dz;</option>
      <option correct="false">x = A(:,1).*dx; y = A(:,1).*dy; z = A(:,1).*dz;</option>
      <option correct="false">x = A(1,:)*dx; y = A(1,:)*dy; z = A(1,:)*dz;</option>
    </optioninput>
  </optionresponse>
  <p>TO COMPLETE #4 :</p>
  <choiceresponse>
    <checkboxgroup>
      <choice correct="false">surf(x,y,z); xlabel('x'); ylabel('y'); zlabel('z');</choice>
      <choice correct="false">plot(x,y); xlabel('x'); ylabel('y');</choice>
      <choice correct="true">plot3(x,y,z); xlabel('x'); ylabel('y'); zlabel('z');</choice>
      <choice correct="false">plot3(x',y',z); xlabel('x'); ylabel('y'); zlabel('z');</choice>
    </checkboxgroup>
  </choiceresponse>
  <solution>
    <div class="detailed-solution">
      <p>Here is the final code</p>
      <pre>
        <code>% 3D random walk

clear; close all;

% Nous voulons dessiner une marche aléatoire en 3D.
% La position initiale n'est pas importante, alors on la définie égale à l'origine

% Le déplacement est décidé de manière aléatoire de la façon suivante:
% soit r une réalisation d'une variable uniformément distribuée entre 0 and 1.
% r  &lt; 1/6   : the displacement is done along parallel to the x-axis, towards the positive values
% 1/6 &lt; r &lt; 2/6 : the displacement is done parallel to the x-axis, towards the negative values
% 2/6 &lt; r &lt; 3/6 : the displacement is done parallel to the y-axis, towards positive values
% 3/6 &lt; r &lt; 4/6 : the displacement is done parallel to the y-axis, towards the negative values
% 4/6 &lt; r &lt; 5/6 : the displacement is done parallel to the z-axis, towards the positive values
% 5/6 &lt; r     : the displacement is done parallel to the z-axis, towards the negative values

% nombre de pas à faire:
steps = 20000;

% First we must generate "steps" random draws
% between 0 and 1 following a uniform distribution
 r = rand(steps,1);


% with the following commands, elementwise,
% the direction of each step can be calculated
 
dx = (r  &lt; 1/6) - ( r &gt; 1/6 &amp; r  &lt;2/6);
dy = (r &gt; 2/6 &amp; r  &lt; 3/6) - ( r &gt; 3/6 &amp; r  &lt;4/6);
dz = (r &gt; 4/6 &amp; r  &lt; 5/6) - ( r &gt; 5/6);


% To sum all steps, one can create the lower triangular matrix
% with only ones below the diagonal
A = tril(ones(steps,steps),-1);

% The incremental sum of each step is performed by A*dx, etc:
x = A*dx; y = A*dy; z = A*dz;

% now (x,y,z) define the successive positions of the random walk

% This walk can be plotted
plot3(x,y,z);
xlabel('x'); ylabel('y'); zlabel('z');
</code>
      </pre>
    </div>
  </solution>
</problem>
