# Introduction to Octave and Matlab

This is the git repo of the course
(MATLAB and Octave for beginners)[https://courses.edx.org/courses/course-v1:EPFLx+MatlabeOctaveBeginnersX+1T2017/course/]


## Course

The source directory is the untarred contents of the edx website (without movies nor discussions). More precisely, to download from edx:

* got to https://studio.edx.org/export/course-v1:EPFLx+MatlabeOctaveBeginnersX+1T2017 and export the course
* remove the local course directory
* untar the course tarball, `tar -xzf ~/Downloads/course.FFFF.tar.gz` 
* check the changes and commit

To upload the local changes to edx:

* first commit all your work
* then download from edx and check that everything is up-to-date
* tar the course directory `tar -czf ~/Downloads/course.tar.gz course/`


## Calerga

The calerga directory is meant for developing the *online octave* corrector.
